# Enunciados
import re
from enum import Enum

# Interações Web
import requests as r

# Análise de texto
import re as regex

# Manipulação de Dados
import xml.dom.minidom
import pandas as pd
import numpy as np
import json

# Tipagem
from collections.abc import Iterable
from typing import (
    List, Tuple, Union
)

# Logs
from loguru import logger as lgg

class SalesforceError(Exception):
    """Base Salesforce API exception"""

    message = 'Unknown error occurred for {url}. Response content: {content}'

    def __init__(self, url, status, resource_name, content):
        """Initialize the SalesforceError exception

        SalesforceError is the base class of exceptions in simple-salesforce

        Args:
            url: Salesforce URL that was called
            status: Status code of the error response
            resource_name: Name of the Salesforce resource being queried
            content: content of the response
        """
        # TODO exceptions don't seem to be using parent constructors at all.
        # this should be fixed.
        # pylint: disable=super-init-not-called
        self.url = url
        self.status = status
        self.resource_name = resource_name
        self.content = content

    def __str__(self):
        return self.message.format(url=self.url, content=self.content)

    def __unicode__(self):
        return self.__str__()

    
class SalesforceAuthenticationFailed(SalesforceError):
    """
    Thrown to indicate that authentication with Salesforce failed.
    """

    def __init__(self, code, message):
        # TODO exceptions don't seem to be using parent constructors at all.
        # this should be fixed.
        # pylint: disable=super-init-not-called
        self.code = code
        self.message = message

    def __str__(self):
        return f'{self.code}: {self.message}'
    
class SoapDados:
    
    """ Classe destinada a realização de manipulações
        do Bancoo de Dados da ORG
    """
    
    query_localizador = ""
    query_campos = []
    objeto = ""
    
    def delete(self, ids: Iterable[list, np.array, pd.DataFrame, pd.Series]):

        """ Método destinado a realização de Deleções ao Banco de Dados da ORG.

            ids: Iterável - Iterável contendo os Ids dos registros para Deleção

        """

        if type(ids) == type(pd.DataFrame()):

            if "ID" not in ids.columns.str.upper():

                raise Exception("O seu DataFrame deve conter uma coluna chamada Id (Case-Insensitive): {}".format(ids.columns))

            ids.columns = ids.columns.str.upper()
            ids = ids["ID"].values

        # Cabeçalho da requisição
        headers: dict = {

            "Content-Type": "text/xml", 
            "SOAPAction": "delete"

        }

        # String destinada a armazenar os registros
        string_registro_completa: str = ""

        for id in ids:

            string_registro_completa += "<urn:ids>{}</urn:ids>".format(id)
        
        # Coleta o XML e adequa os respectivos campos
        xml_delete = regex.sub("SESSAO_ID", self.sessao_id, SalesforceConfiguracoes.XML_DELETE.value)
        xml_delete = regex.sub("REGISTROS", string_registro_completa, xml_delete)

        # Realiza a requisição junto ao serviço
        resposta = r.post(self.org_instancia_url + SalesforceEndpoints.SERVICO_SOAP.value, 
                     data=xml_delete, 
                     headers=headers
                    )

        if resposta.status_code != 200:

            codigo_excecao = Utilidades.retorna_valorelemento_pornome(
                                    resposta.content, 'sf:exceptionCode'
                                 )

            msg_excecao = Utilidades.retorna_valorelemento_pornome(
                                resposta.content, 'sf:exceptionMessage'
                              )

            raise Exception(codigo_excecao, msg_excecao)

        elif Utilidades.retorna_valorelemento_pornome(
                    resposta.content, 'success'
             ) == 'false':

            codigo_excecao = Utilidades.retorna_valorelemento_pornome(
                                    resposta.content, 'fields'
                                 )

            msg_excecao = Utilidades.retorna_valorelemento_pornome(
                                resposta.content, 'message'
                              )

            raise Exception(codigo_excecao, msg_excecao)

        return resposta
    
    def insert(self, df: pd.DataFrame, nome_objeto: str):

        """ Método destinado a realização de Inserções ao Banco de Dados da ORG.

                df: pd.DataFrame - DataFrame contendo os registros para Inserção
                nome_objeto: str - Nome do Objeto para Inserção

        """

        # Cabeçalho da requisição
        headers: dict = {

            "Content-Type": "text/xml", 
            "SOAPAction": "create"

        }

        # String destinada a armazenar os registros
        string_registro_parcial: str = "<urn:sObjects><urn1:type>NOME_OBJETO</urn1:type>DADOS</urn:sObjects>".replace("NOME_OBJETO", nome_objeto)

        # Aloca uma list para cada registro
        string_registros_parcial: list = ["" for _ in range(len(df))]
        # Monta um campo XML para cada
        # valor do DataFrame
        for coluna in df.columns:

            i = 0
            for valor in df[coluna].astype(str):

                string_campos = ""
                string_campos += f"<{coluna}>" + valor + f"</{coluna}>"
                string_registros_parcial[i] += "" + string_campos if len(string_registros_parcial[i]) > 0 else string_campos

                i += 1

        # Adequa os campos aos padrões do XML
        for indice in range(len(string_registros_parcial)):

            string_registros_parcial[indice] = string_registro_parcial.replace("DADOS", string_registros_parcial[indice])

        string_registros_completa = ""
        for registro in string_registros_parcial:

            string_registros_completa += registro + ""

        # Coleta o XML e adequa os respectivos campos
        xml_insert = regex.sub("SESSAO_ID", self.sessao_id, SalesforceConfiguracoes.XML_INSERT.value)
        xml_insert = regex.sub("REGISTROS", string_registros_completa, xml_insert)

        # Realiza a requisição junto ao serviço
        resposta = r.post(self.org_instancia_url + SalesforceEndpoints.SERVICO_SOAP.value, 
                     data=xml_insert, 
                     headers=headers
                    )

        if resposta.status_code != 200:

            codigo_excecao = Utilidades.retorna_valorelemento_pornome(
                                    resposta.content, 'sf:exceptionCode'
                                 )

            msg_excecao = Utilidades.retorna_valorelemento_pornome(
                                resposta.content, 'sf:exceptionMessage'
                              )

            raise Exception(codigo_excecao, msg_excecao)

        elif Utilidades.retorna_valorelemento_pornome(
                    resposta.content, 'success'
             ) == 'false':
            
            codigo_excecao = Utilidades.retorna_valorelemento_pornome(
                                    resposta.content, 'fields'
                                 )
            
            msg_excecao = Utilidades.retorna_valorelemento_pornome(
                                resposta.content, 'message'
                              )

            raise Exception(codigo_excecao, msg_excecao)
        
        return resposta
    
    def undelete(self, ids: Iterable[list, np.array, pd.DataFrame, pd.Series]):

        """ Método destinado a realização de Recuperação Deleções ao Banco de Dados da ORG.

            ids: Iterável - Iterável contendo os Ids dos registros para Recuperação de Deleção

        """

        if type(ids) == type(pd.DataFrame()):

            if "ID" not in ids.columns.str.upper():

                raise Exception("O seu DataFrame deve conter uma coluna chamada Id (Case-Insensitive): {}".format(ids.columns))

            ids.columns = ids.columns.str.upper()
            ids = ids["ID"].values

        # Cabeçalho da requisição
        headers: dict = {

            "Content-Type": "text/xml", 
            "SOAPAction": "undelete"

        }

        # String destinada a armazenar os registros
        string_registro_completa: str = ""

        for id in ids:

            string_registro_completa += "<urn:ids>{}</urn:ids>".format(id)
        
        # Coleta o XML e adequa os respectivos campos
        xml_undelete = regex.sub("SESSAO_ID", self.sessao_id, SalesforceConfiguracoes.XML_UNDELETE.value)
        xml_undelete = regex.sub("REGISTROS", string_registro_completa, xml_undelete)

        # Realiza a requisição junto ao serviço
        resposta = r.post(self.org_instancia_url + SalesforceEndpoints.SERVICO_SOAP.value, 
                     data=xml_undelete, 
                     headers=headers
                    )

        if resposta.status_code != 200:

            codigo_excecao = Utilidades.retorna_valorelemento_pornome(
                                    resposta.content, 'sf:exceptionCode'
                                 )

            msg_excecao = Utilidades.retorna_valorelemento_pornome(
                                resposta.content, 'sf:exceptionMessage'
                              )

            raise Exception(codigo_excecao, msg_excecao)

        elif Utilidades.retorna_valorelemento_pornome(
                    resposta.content, 'success'
             ) == 'false':

            codigo_excecao = Utilidades.retorna_valorelemento_pornome(
                                    resposta.content, 'fields'
                                 )

            msg_excecao = Utilidades.retorna_valorelemento_pornome(
                                resposta.content, 'message'
                              )

            raise Exception(codigo_excecao, msg_excecao)

        return resposta
    
    def update(self, df: pd.DataFrame, nome_objeto: str):

        """ Método destinado a realização de Atualizações ao Banco de Dados da ORG.

                df: pd.DataFrame - DataFrame contendo os registros para Inserção
                nome_objeto: str - Nome do Objeto para Inserção

        """
        
        if "ID" not in df.columns.str.upper():
            
            raise Exception("O seu DataFrame deve conter uma coluna chamada Id (Case-Insensitive): {}".format(df.columns))
            
        # Cabeçalho da requisição
        headers: dict = {

            "Content-Type": "text/xml", 
            "SOAPAction": "update"

        }

        # String destinada a armazenar os registros
        string_registro_parcial: str = "<urn:sObjects><urn1:type>NOME_OBJETO</urn1:type>DADOS</urn:sObjects>".replace("NOME_OBJETO", nome_objeto)

        # Aloca uma list para cada registro
        string_registros_parcial: list = ["" for _ in range(len(df))]
        # Monta um campo XML para cada
        # valor do DataFrame
        for coluna in df.columns:

            i = 0
            for valor in df[coluna].astype(str):

                string_campos = ""
                string_campos += f"<{coluna}>" + valor + f"</{coluna}>"
                string_registros_parcial[i] += "" + string_campos if len(string_registros_parcial[i]) > 0 else string_campos

                i += 1

        # Adequa os campos aos padrões do XML
        for indice in range(len(string_registros_parcial)):

            string_registros_parcial[indice] = string_registro_parcial.replace("DADOS", string_registros_parcial[indice])

        string_registros_completa = ""
        for registro in string_registros_parcial:

            string_registros_completa += registro + ""

        # Coleta o XML e adequa os respectivos campos
        xml_insert = regex.sub("SESSAO_ID", self.sessao_id, SalesforceConfiguracoes.XML_UPDATE.value)
        xml_insert = regex.sub("REGISTROS", string_registros_completa, xml_insert)

        # Realiza a requisição junto ao serviço
        resposta = r.post(self.org_instancia_url + SalesforceEndpoints.SERVICO_SOAP.value, 
                     data=xml_insert, 
                     headers=headers
                    )

        if resposta.status_code != 200:

            codigo_excecao = Utilidades.retorna_valorelemento_pornome(
                                    resposta.content, 'sf:exceptionCode'
                                 )

            msg_excecao = Utilidades.retorna_valorelemento_pornome(
                                resposta.content, 'sf:exceptionMessage'
                              )

            raise Exception(codigo_excecao, msg_excecao)

        elif Utilidades.retorna_valorelemento_pornome(
                    resposta.content, 'success'
             ) == 'false':
            
            codigo_excecao = Utilidades.retorna_valorelemento_pornome(
                                    resposta.content, 'fields'
                                 )
            
            msg_excecao = Utilidades.retorna_valorelemento_pornome(
                                resposta.content, 'message'
                              )

            raise Exception(codigo_excecao, msg_excecao)
        
        return resposta
    
    def query(self, query_string="", tamanho_match_query=None):
    
        """ Método destinado a realização de consultas ao Banco de Dados da ORG.
            
            query_string: str - Consulta
            tamanho_match_query: str - Tamanho da Batch para execução
            
        """
        
        headers = {

            "Content-Type": "text/xml", 
            "SOAPAction": "queryMore"

        }
        
        query_string = query_string.upper()
        if self.query_localizador not in ["", '<queryLocator xsi:nil="true"/>']:
            
            xml_query = SalesforceConfiguracoes.XML_QUERY_MORE.value
            xml_query = regex.sub("QUERY_LOCALIZADOR", self.query_localizador, xml_query)
            
        else:
            
            xml_query = SalesforceConfiguracoes.XML_QUERY_ALL.value
            xml_query = regex.sub("QUERY_STRING", query_string, xml_query)
        
        # Coleta o XML e adequa os respectivos dados
        xml_query = regex.sub("SESSAO_ID", self.sessao_id, xml_query)
        tamanho_match_query = SalesforceConfiguracoes.TAMANHO_BATCH_QUERY.value if tamanho_match_query is None else tamanho_match_query
        xml_query = regex.sub("TAMANHO_BATCH_QUERY", tamanho_match_query, xml_query)
        
        # Realiza a requisição junto ao serviço
        resposta = r.post(self.org_instancia_url + SalesforceEndpoints.SERVICO_SOAP.value, 
                     data=xml_query, 
                     headers=headers
                    )

        open("tmp.txt", "w").write(resposta.text)
        if resposta.status_code != 200:

            codigo_excecao = Utilidades.retorna_valorelemento_pornome(
                                    resposta.content, 'sf:exceptionCode'
                                 )

            msg_excecao = Utilidades.retorna_valorelemento_pornome(
                                resposta.content, 'sf:exceptionMessage'
                              )

            raise Exception(codigo_excecao, msg_excecao)
            
        query_campos = []
        if query_string:
            # if destinado a somente coletar os campos da query quando uma query_string
            # for passada. Visto que o método utiliza de recursão para realização da repaginação
            self.query_campos = Utilidades.get_campos_query(query_string)

        # Coleta os dados da query e armazena no pandas.DataFrame: df_query_dados 
        df_query_dados = pd.DataFrame()
        for campo in self.query_campos:

            df_query_dados[campo] = Utilidades.valores_campos_xml(campo, resposta.text)

        self.query_localizador = Utilidades.retorna_valorelemento_pornome(xml_string=resposta.text, nome_elemento="queryLocator")
        
        while self.query_localizador not in ["", '<queryLocator xsi:nil="true"/>']:
            
            df = self.query()            
            df_query_dados = pd.concat([df_query_dados, df])
            
        self.query_localizador = ""
        self.query_campos = []
        
        return df_query_dados
    
class SoapAuth:
    
    """ Classe destinada a realização de login e logout da ORG
    """
    
    def __init__(self, username, senha, secret, url=None):
        
        self.username = username
        self.senha, self.secret = senha, secret
    
        self.url = SalesforceEndpoints.URL_SOAP_PADRAO.value if url is None else url 
        self.url_instancia_soap = "" # URL da instância concatenado com o endpoint SalesforceEndpoints.SERVICO_SOAP.value. * Inicializado no método __login()
        self.login()
        
    def login(self):
        
        """ Método destinado a realização da Autenticação/Login junto a Salesforce API Soap.
            
        """
        
        # Coleta o XML e adequa os respectivos dados
        xml_login = regex.sub("USERNAME", self.username, SalesforceConfiguracoes.XML_LOGIN.value)
        xml_login = regex.sub("SENHA_SECRET", self.senha + self.secret, xml_login)
        
        headers = {

            "Content-Type": "text/xml; charset=UTF-8",
            "SOAPAction": "login",
            "Accept": "text/xml"

        }
        
        # Realiza a requisição junto ao serviço
        resposta = r.post(self.url, headers=headers, data=xml_login)

        if resposta.status_code != 200:
            
            codigo_excecao = Utilidades.retorna_valorelemento_pornome(
                                resposta.content, 'sf:exceptionCode'
                             )
            
            msg_excecao = Utilidades.retorna_valorelemento_pornome(
                            resposta.content, 'sf:exceptionMessage'
                          )
            
            raise SalesforceAuthenticationFailed(codigo_excecao, msg_excecao)

        sessao_id = Utilidades.retorna_valorelemento_pornome(
                        resposta.content, 'sessionId'
                    )
        org_instancia_url = Utilidades.retorna_valorelemento_pornome(
                        resposta.content, 'serverUrl'
                    )
        
        org_instancia_url = org_instancia_url.split("/services")[0]
        
        self.url_instancia_soap = org_instancia_url + SalesforceEndpoints.SERVICO_SOAP.value
        self.sessao_id = sessao_id; self.org_instancia_url = org_instancia_url
        

    def logout(self):
        
        """ Método destinado ao encerramento da Autenticação/Login junto a Salesforce API Soap.
            
        """

        headers = {

            "Content-Type": "text/xml; charset=UTF-8",
            "SOAPAction": "logout",

        }
        
        # Coleta o XML e adequa os respectivos dados
        xml_logout = regex.sub("SESSAO_ID", self.sessao_id, SalesforceConfiguracoes.XML_LOGOUT.value)
        
        # Realiza a requisição junto ao serviço
        resposta = r.post(self.url_instancia_soap, headers=headers, data=xml_logout)
        
        if resposta.status_code != 200:
            
            codigo_excecao = Utilidades.retorna_valorelemento_pornome(
                                resposta.content, 'sf:exceptionCode'
                            )
            
            msg_excecao = Utilidades.retorna_valorelemento_pornome(
                            resposta.content, 'sf:exceptionMessage'
                        )
            
            raise SalesforceAuthenticationFailed(codigo_excecao, msg_excecao)

                 

        self.sessao_id = ""

class SalesSoap(SoapDados, SoapAuth):
            
    """ Classe destinada a armazenar dados de sessão e métodos para interação com a API
    
    """
    
    def __init__(self, username, senha, secret, url=None):
        
        super().__init__(username, senha, secret, url=None)
            
class SalesforceConfiguracoes(Enum):
    
    """ Classe destinada a armazenar contantes de configuração 
    
    """
    
    VERSAO_API_PADRAO = "58.0"
    TIPO_LOGIN_PADRAO = "u" # utilizar "c" para partner
    TAMANHO_BATCH_QUERY = "50"

    # XML para realização de Exclusões
    XML_DELETE = """
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:partner.soap.sforce.com">
   <soapenv:Header>
      <urn:SessionHeader>
         <urn:sessionId>SESSAO_ID</urn:sessionId>
      </urn:SessionHeader>
   </soapenv:Header>
   <soapenv:Body>
      <urn:delete>
         REGISTROS
      </urn:delete>
   </soapenv:Body>
</soapenv:Envelope>
"""
    
    # XML para realização de Inserções
    XML_INSERT = """
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:partner.soap.sforce.com" xmlns:urn1="urn:sobject.partner.soap.sforce.com">
   <soapenv:Header>
      <urn:SessionHeader>
         <urn:sessionId>SESSAO_ID</urn:sessionId>
      </urn:SessionHeader>
   </soapenv:Header>
   <soapenv:Body>
      <urn:create>
         REGISTROS
      </urn:create>
   </soapenv:Body>
</soapenv:Envelope>
"""
    
    # XML para realização de Atualizações
    XML_UPDATE = """
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:partner.soap.sforce.com" xmlns:urn1="urn:sobject.partner.soap.sforce.com">
   <soapenv:Header>
       <urn:SessionHeader>
         <urn:sessionId>SESSAO_ID</urn:sessionId>
      </urn:SessionHeader>
   </soapenv:Header>
   <soapenv:Body>
      <urn:update>
         REGISTROS
      </urn:update>
   </soapenv:Body>
</soapenv:Envelope>
"""
    
    # XML para realização de Recuperação de Exclusões
    XML_UNDELETE = """
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:partner.soap.sforce.com">
   <soapenv:Header>
      <urn:SessionHeader>
         <urn:sessionId>SESSAO_ID</urn:sessionId>
      </urn:SessionHeader>
   </soapenv:Header>
   <soapenv:Body>
      <urn:undelete>
        REGISTROS
      </urn:undelete>
   </soapenv:Body>
</soapenv:Envelope>
"""
    
        # XML para realização de login
    XML_LOGIN = """
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:partner.soap.sforce.com">
   <soapenv:Body>
      <urn:login>
         <urn:username>USERNAME</urn:username>
         <urn:password>SENHA_SECRET</urn:password>
      </urn:login>
   </soapenv:Body>
</soapenv:Envelope>
"""
    
    # XML para realização de logout
    XML_LOGOUT = """
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:partner.soap.sforce.com">
   <soapenv:Header>
      <urn:SessionHeader>
         <urn:sessionId>SESSAO_ID</urn:sessionId>
      </urn:SessionHeader>
   </soapenv:Header>
   <soapenv:Body>
      <urn:logout/>
   </soapenv:Body>
</soapenv:Envelope>
"""

    # XML para realização de Querys
    XML_QUERY_ALL = """
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:partner.soap.sforce.com">
   <soapenv:Header>
      <urn:QueryOptions>
         <urn:batchSize>TAMANHO_BATCH_QUERY</urn:batchSize>
      </urn:QueryOptions>
      <urn:SessionHeader>
         <urn:sessionId>SESSAO_ID</urn:sessionId>
      </urn:SessionHeader>
   </soapenv:Header>
   <soapenv:Body>
      <urn:queryAll>
         <urn:queryString>QUERY_STRING</urn:queryString>
      </urn:queryAll>
   </soapenv:Body>
</soapenv:Envelope>
"""
    # XML para realização de Querys
    XML_QUERY_MORE = """
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:partner.soap.sforce.com">
   <soapenv:Header>
      <urn:QueryOptions>
         <urn:batchSize>TAMANHO_BATCH_QUERY</urn:batchSize>
      </urn:QueryOptions>
      <urn:SessionHeader>
         <urn:sessionId>SESSAO_ID</urn:sessionId>
      </urn:SessionHeader>
   </soapenv:Header>
   <soapenv:Body>
      <urn:queryMore>
         <urn:queryLocator>QUERY_LOCALIZADOR</urn:queryLocator>
      </urn:queryMore>
   </soapenv:Body>
</soapenv:Envelope>
"""
    
class SalesforceEndpoints(Enum):
    
    """ Classe destinada a armazenar URLs dos Endpoints SOAP
    
    """
    
    URL_SOAP_PADRAO = f"https://login.salesforce.com/services/Soap/{SalesforceConfiguracoes.TIPO_LOGIN_PADRAO.value}/{SalesforceConfiguracoes.VERSAO_API_PADRAO.value}"
    
    SERVICO_SOAP = "/services/Soap" + "/" + SalesforceConfiguracoes.TIPO_LOGIN_PADRAO.value + "/" + SalesforceConfiguracoes.VERSAO_API_PADRAO.value
    
    VERSAO_API_PADRAO = "58.0" 

class Utilidades:
    
    """ Classe destinada a armazenar métodos auxiliares para a biblioteca
    """

    @staticmethod
    def get_campos_query(query_str: str) -> List[str]:
        """
        :param query_str: String da query
        :return: Lista dos campos da string da query
        """
        # Coleta os campos solicitados na String de Consulta. Sendo estes mais à frente coletados junto ao retorno,
        # visto que todo sistema é falho e não é desejável dados não solicitados
        span: Tuple[int] = re.search(re.compile("FROM [a-zA-Z]*"), query_str).span()
        return [

            campo.strip() for campo in
            query_str[:span[0]].removeprefix("SELECT").strip().replace(" ", "").split(",")

        ]

    @staticmethod
    def campo_lookup(nome_campo: str) -> bool:
        return True if "." in nome_campo else False

    @staticmethod
    def get_val_campo_normal(nome_campo: str, registro_xml_string: str) -> str:
        """
            :param nome_campo: XML para manipulação
            :param registro_xml_string: Nome da TAG para a procura
            :return: Retorna o valor de um campo contido no xml passado como parâmetro
        """
        campo_xml_ini = f"<sf:{nome_campo}>"
        campo_xml_fim = f"<\/sf:{nome_campo}>"

        val = re.findall(f"{campo_xml_ini}.*?{campo_xml_fim}", registro_xml_string, re.IGNORECASE)[0]
        val = val[len(campo_xml_ini):]  # Remove a tag inicial do xml
        val = val[:len(val) - len(campo_xml_ini) - 1]  # Remove a tag final do xml
        return val

    @staticmethod
    def get_val_campo_lookup(nome_campo: str, registro_xml_string: str) -> Union[np.nan, str]:
        """
            :param nome_campo: XML para manipulação
            :param registro_xml_string: Nome da TAG para a procura
            :return: Retorna o valor de um campo  Lookup contido no xml passado como parâmetro
        """
        campos_compostos = nome_campo.split(".")
        campo_alvo = campos_compostos.pop()
        campo_xml_ini = f"<sf:{campo_alvo}>"
        campo_xml_fim = f"<\/sf:{campo_alvo}>"

        while campos_compostos:
            campo_alvo_tmp = campos_compostos.pop()
            if re.search('<sf:' + campo_alvo + ">", registro_xml_string, re.IGNORECASE):
                # Se o registro possuir uma conta
                string_procura = '<sf:' + campo_alvo_tmp + ' xsi:type="sf:sObject">.*?</sf:' + campo_alvo_tmp + '>'
            else:
                string_procura = '<sf:' + campo_alvo_tmp + ' xsi:nil="true"' + '>'

            pre_val = re.search(string_procura, registro_xml_string, re.IGNORECASE)
            if pre_val is None:
                return np.nan
            pre_val = pre_val.group()

        val = re.search(f"{campo_xml_ini}.*?{campo_xml_fim}", pre_val, re.IGNORECASE).group()
        val = val[len(campo_xml_ini):]  # Remove a tag inicial do xml
        val = val[:len(val) - len(campo_xml_ini) - 1]  # Remove a tag final do xml

        return val

    @staticmethod
    def retorna_valorelemento_pornome(xml_string, nome_elemento, erro=False) -> Union[str, None]:
        """
            :param xml_string: XML para manipulação
            :param nome_elemento: Nome da TAG para a procura
            :param erro: Permite ou não a recursão de mais de uma tags no caso de um xml de erro
            :return: Retorna o conteúdo interno de um XML se encontrado, caso contrário, retorna None
        """
        
        xmlDom = xml.dom.minidom.parseString(xml_string)
        elementos = xmlDom.getElementsByTagName(nome_elemento)
        
        if len(elementos) > 0:
            valores_elemento = (
                elementos[0]
                .toxml()
                .replace('<' + nome_elemento + '>', '')
                .replace('</' + nome_elemento + '>', '')
            )
        
        elif len(elementos) == 0 and erro:

            nome_elemento = "faultcode" if nome_elemento == "sf:exceptionCode" else "faultstring"
            valores_elemento = Utilidades.retorna_valorelemento_pornome(xml_string, nome_elemento, erro=erro)

        return valores_elemento if len(elementos) > 0 else None
    
    @staticmethod
    def valores_campos_xml(nome_campo: str, xml_string: str) -> Union[List[str], List[int]]:
        """
           :param nome_campo: Nome do campo para procura no xml
           sem suas as tags xml
           :param xml_string: String xml para extração do valor do campo
           :return: Lista dos campos do xml passado
        """
        registros = re.findall('<records xsi:type="sf:sObject">.*?</records>', xml_string)

        if not Utilidades.campo_lookup(nome_campo) and (re.search(nome_campo, "".join(registros), re.IGNORECASE)  == None):
            raise Exception(f"O campo '{nome_campo}' não está presente na 'xml_strig': {xml_string[0:len(xml_string)//10]}...")

        lista_valores = []
        for reg in registros:
            if Utilidades.campo_lookup(nome_campo):
                val = Utilidades.get_val_campo_lookup(nome_campo, reg)
            else:
                val = Utilidades.get_val_campo_normal(nome_campo, reg)
            lista_valores.append(val)
        return lista_valores


if __name__ == "__main__":
    cred = json.load(open("cred.json", "r"))
    client = SalesSoap(cred["username"], cred["senha"], cred["secret"])
    df = client.query("SELECT ID, PROFILE.USERLICENSE.NAME FROM USER")
    print(df)